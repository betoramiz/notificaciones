﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SistemaNotificaciones.ViewModel
{
    public class CorreoVM
    {
        public int IdContrato { get; set; }
        public string NumeroContrato { get; set; }
        public int IdPropiedad { get; set; }
        public string NumeroCasa { get; set; }
        public string NombrePropietario { get; set; }
        public string Contacto { get; set; }
        public string Email { get; set; }
        public DateTime FechaInicio { get; set; }
        public DateTime FechaFin { get; set; }
        public int Dias { get; set; }
    }
}