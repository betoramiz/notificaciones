﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SistemaNotificaciones.ViewModel
{
    public class RenovarVM
    {
        public string Token { get; set; }
        public string Contrato { get; set; }

        public int ServicioId { get; set; }
        public List<Models.Servicio> Servicios { get; set; }
    }
}