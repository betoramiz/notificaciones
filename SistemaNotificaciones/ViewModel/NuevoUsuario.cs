﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNet.Identity.EntityFramework;

namespace SistemaNotificaciones.ViewModel
{
    public class NuevoUsuario
    {
        [Required(ErrorMessage = "Este Campo es Requerido")]
        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }

        [Required(ErrorMessage = "Este campo es Requerido")]
        public int RoleId { get; set; }

        [Display(Name ="Role")]
        public List<IdentityRole> Roles { get; set; }

        [Required(ErrorMessage = "Este Campo es Requerido")]
        [DataType(DataType.Password)]
        [Display(Name ="Contrasena")]
        public string Password { get; set; }

        [Required(ErrorMessage = "Este Campo es Requerido")]
        [DataType(DataType.Password)]
        [Compare("Password")]
        [Display(Name = "Confirmar Contrasena")]
        public string ConfirmPassword { get; set; }
    }
}

public class RoleClass
{
    public int Id { get; set; }
    public string Name { get; set; }
}