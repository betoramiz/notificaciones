﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SistemaNotificaciones.Models;

namespace SistemaNotificaciones.ViewModel
{
    public class TableroVM
    {
        public int Vencidos { get; set; }
        public int PorVencer { get; set; }
        public int Activos { get; set; }

        public List<Contrato> Contratos { get; set; }
    }
}