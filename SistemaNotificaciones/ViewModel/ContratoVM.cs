﻿using SistemaNotificaciones.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SistemaNotificaciones.ViewModel
{
    public class ContratoVM
    {
        public int Id { get; set; }

        [Required]
        public string Numero { get; set; }

        [Required]
        [DataType(DataType.Date)]
        public DateTime FechaIncio { get; set; }

        [Required]
        [DataType(DataType.Date)]
        public DateTime FechaFin { get; set; }

        [MaxLength(2000)]
        public string Observaciones { get; set; }

        public bool Activo { get; set; }

        [DataType(DataType.ImageUrl)]
        public string RutaArchivos { get; set; }

        [Required]
        public string NumeroPropiedad { get; set; }
        public List<Propiedad> Propiedades { get; set; }

        [Required]
        public int servicioId { get; set; }
        public List<Servicio> Servicios { get; set; }

        [Required]
        public int cobranzaId { get; set; }
        public List<Cobranza> Cobranzas { get; set; }
    }
}