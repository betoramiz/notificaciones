﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SistemaNotificaciones.ViewModel
{
    public class ServicioVM
    {
        [Key]
        public int Id { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "El Nombre del Servicio no puede estar vacio")]
        [MaxLength(50)]
        public string Nombre { get; set; }

        [Required]
        public int Duracion { get; set; }

        public string MedicioId { get; set; }

        public List<string> Medicion { get; set; }
    }
}