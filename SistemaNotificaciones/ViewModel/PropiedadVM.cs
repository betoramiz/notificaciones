﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SistemaNotificaciones.ViewModel
{
    public class PropiedadVM
    {
        public int Id { get; set; }
        public string Numero { get; set; }
        public string Propietario { get; set; }
        public int IdManager { get; set; }
        public string Manager { get; set; }
    }
}