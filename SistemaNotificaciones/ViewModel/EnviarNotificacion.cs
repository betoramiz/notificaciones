﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SistemaNotificaciones.ViewModel
{
    public class EnviarNotificacion
    {
        public int IdContrato { get; set; }
        [Display(Name = "Propietario")]
        public string NombrePropietario { get; set; }
        [Display(Name = "Numero de Propiedad")]
        public string NumeroPropiedad { get; set; }

        public string Token { get; set; }

        [Display(Name = "Correo Electronico")]
        public string Email { get; set; }
        [Required]
        public string Mensaje { get; set; }

        [Display(Name = "Adjuntar Archivo")]
        public HttpPostedFileBase FileAttached { get; set; }
    }
}