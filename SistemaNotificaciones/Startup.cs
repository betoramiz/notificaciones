﻿using System;
using System.Threading.Tasks;
using Microsoft.Owin;
using Owin;
using Microsoft.Owin.Security.Cookies;
using Microsoft.AspNet.Identity;
using SistemaNotificaciones.Models;

[assembly: OwinStartup(typeof(SistemaNotificaciones.Startup))]

namespace SistemaNotificaciones
{
    public class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            //app.CreatePerOwinContext(Contexto.Create());
            //app.CreatePerOwinContext<UsuarioSistema>()


            CookieAuthenticationOptions options = new CookieAuthenticationOptions();
            options.AuthenticationType = DefaultAuthenticationTypes.ApplicationCookie;
            options.LoginPath = new PathString("/Security/LogIn");
            app.UseCookieAuthentication(options);
        }
    }
}
