﻿using System;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using SistemaNotificaciones.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace SistemaNotificaciones.Filters
{
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method)]
    public class CustomAuthorize :  AuthorizeAttribute
    {
        public string EnabledRoles { get; set; }
        public bool AllowAnonimous { get; set; }

        private Contexto _contexto = null;
        private Contexto contexto
        {
            get
            {
                if (_contexto == null)
                    _contexto = new Contexto();
                return _contexto;
            }
        }

        private UserStore<UsuarioSistema> _userStore = null;
        private UserStore<UsuarioSistema> userStore
        {
            get
            {
                if (_userStore == null)
                    _userStore = new UserStore<UsuarioSistema>(contexto);

                return _userStore;
            }
        }

        private UserManager<UsuarioSistema> _userManager = null;
        public UserManager<UsuarioSistema> userManager
        {
            get
            {
                if (_userManager == null)
                    _userManager = new UserManager<UsuarioSistema>(userStore);

                return _userManager;
            }
        }

        protected override bool AuthorizeCore(HttpContextBase httpContext)
        {
            //bool isAuhorized =  base.AuthorizeCore(httpContext);
            if(httpContext.User.Identity.IsAuthenticated)
            {
                var usuario = userManager.FindByName(httpContext.User.Identity.Name).Roles.FirstOrDefault();

                var roleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(contexto));
                var role = roleManager.FindById(usuario.RoleId.ToString());

                
                if (EnabledRoles.Split(',').Select(x => x.Trim()).Any(x => x == role.Name))
                    return true;
                else
                {
                    httpContext.Response.RedirectLocation = "~/Security/NoAutorizado";
                    return true;
                }
            }
            else
            {
                if (AllowAnonimous)
                {
                    httpContext.SkipAuthorization = true;
                    return true;
                }
                httpContext.Response.RedirectLocation = "~/Security/LogIn";
                return false;
            }
        }

        //protected override void HandleUnauthorizedRequest(AuthorizationContext filterContext)
        //{
        //    if(!filterContext.HttpContext.User.Identity.IsAuthenticated)
        //        filterContext.Result = new RedirectResult("~/Security/LogIn");

        //    filterContext.Result = new RedirectResult("~/Security/NoAutorizado");
        //}

    }
}