namespace SistemaNotificaciones.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class renovaciones : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Renovaciones",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ContratoId = c.Int(nullable: false),
                        Renovo = c.Boolean(nullable: false),
                        FechaRenovacion = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.Renovaciones");
        }
    }
}
