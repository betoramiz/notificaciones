using System.Web.Mvc;
using SistemaNotificaciones.ViewModel;
using Microsoft.AspNet.Identity;
using System.Net;
using Microsoft.Owin;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.AspNet.Identity.EntityFramework;
using SistemaNotificaciones.Models;
using System.Web;
using System.Linq;
using System;

namespace SistemaNotificaciones.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;
    using System.Web;
    internal sealed class Configuration : DbMigrationsConfiguration<SistemaNotificaciones.Models.Contexto>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
            
        }

        protected override void Seed(SistemaNotificaciones.Models.Contexto context)
        {
            if (!(context.Users.Any(u => u.UserName == "admin@admin.com")))
            {
                var userStore = new UserStore<UsuarioSistema>(context);
                var userManager = new UserManager<UsuarioSistema>(userStore);
                var userToInsert = new UsuarioSistema { UserName = "admin@admin.com", Email = "admin@admin.com" };
                var result = userManager.Create(userToInsert, "Adm1n123");

                var roleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(context));
                if(!roleManager.RoleExists("Admin"))
                {
                    roleManager.Create(new IdentityRole("Admin"));
                }
                if (!roleManager.RoleExists("Normal"))
                {
                    roleManager.Create(new IdentityRole("Normal"));
                }

                var user = userManager.FindByEmail("admin@admin.com");
                userManager.AddToRole(user.Id, "Admin");
            }
        }
    }
}
