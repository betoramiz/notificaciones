﻿using SistemaNotificaciones.Models;
using SistemaNotificaciones.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SistemaNotificaciones.Bussiness
{
    public class ManagerBusiness : Repository<Manager>
    {
        public ManagerBusiness(Contexto contexto) : base(contexto) { }
    }
}