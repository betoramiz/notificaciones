﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SistemaNotificaciones.Repository;
using SistemaNotificaciones.Models;

namespace SistemaNotificaciones.Bussiness
{
    public class RenovacionBusiness : Repository<Renovacion>
    {
        public RenovacionBusiness(Contexto contexto) : base(contexto) { }
    }
}