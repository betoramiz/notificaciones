﻿using SistemaNotificaciones.Models;
using SistemaNotificaciones.Repository;
using System.Collections.Generic;

namespace SistemaNotificaciones.Bussiness
{
    public class PropiedadBusiness : Repository<Propiedad>
    {
        public PropiedadBusiness(Contexto contexto) : base(contexto) { }
    }
}