﻿using SistemaNotificaciones.Models;
using SistemaNotificaciones.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SistemaNotificaciones.Bussiness
{
    public class ContratoBusiness : Repository<Contrato>
    {
        public ContratoBusiness(Contexto context) : base(context) { }
    }
}