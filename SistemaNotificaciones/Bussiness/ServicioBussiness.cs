﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SistemaNotificaciones.Repository;
using SistemaNotificaciones.Models;

namespace SistemaNotificaciones.Bussiness
{
    public class ServicioBussiness : Repository<Servicio>
    {
        public ServicioBussiness(Contexto contexto) : base(contexto) { }
    }
}