﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace SistemaNotificaciones.Models
{
    [Table("Renovaciones")]
    public class Renovacion
    {
        [Key]
        public int Id { get; set; }
        public int ContratoId { get; set; }
        public bool Renovo { get; set; }
        public DateTime FechaRenovacion { get; set; }
    }
}