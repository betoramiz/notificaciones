﻿using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace SistemaNotificaciones.Models
{
    public class Contexto : IdentityDbContext<UsuarioSistema>
    {
        public Contexto() : base("name=SQlConnection") {}

        public static Contexto Create()
        {
            return new Contexto();
        }

        public DbSet<Cobranza> Cobranzas { get; set; }
        public DbSet<Contrato> Contratos { get; set; }
        public DbSet<Manager> Managers { get; set; }
        public DbSet<Propiedad> Propiedades { get; set; }
        public DbSet<Servicio> Servicios { get; set; }
        public DbSet<Renovacion> Renovaciones { get; set; }
    }
}