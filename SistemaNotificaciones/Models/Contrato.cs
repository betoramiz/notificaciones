﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace SistemaNotificaciones.Models
{
    [Table("Contratos")]
    public class Contrato
    {
        public Contrato()
        {
            Activo = true;
        }

        [Key]
        public int Id { get; set; }

        [Required]
        public string Numero { get; set; }

        [Required]
        public DateTime FechaIncio { get; set; }

        [Required]
        public DateTime FechaFin { get; set; }

        [MaxLength(2000)]
        public string Observaciones { get; set; }

        public bool Activo { get; set; }

        [DataType(DataType.ImageUrl)]
        public string RutaArchivos { get; set; }

        public bool NotificacionEnviada { get; set; }
        public string Token { get; set; }

        public int PropiedadId { get; set; }
        public Propiedad Propiedad { get; set; }

        public int ServicioId { get; set; }
        public Servicio Servicio { get; set; }

        public int CobranzaId { get; set; }
        public Cobranza Cobranza { get; set; }
    }
}