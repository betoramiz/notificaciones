﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace SistemaNotificaciones.Models
{
    [Table("Servicios")]
    public class Servicio
    {
        [Key]
        public int Id { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "El Nombre del Servicio no puede estar vacio")]
        [MaxLength(50)]
        public string Nombre { get; set; }

        [Required]
        public int Duracion { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "La medicion del Servicio no puede estar vacia")]
        [MaxLength(20)]
        public string Medicion { get; set; }
    }
}