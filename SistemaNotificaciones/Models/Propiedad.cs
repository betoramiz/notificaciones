﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SistemaNotificaciones.Models
{
    [Table("Propiedades")]
    public class Propiedad
    {
        [Key]
        public int Id { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "El campo Numero no puede estar Vacio")]
        [MaxLength(20)]
        public string Numero { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "El nombre del propietario no puede estar Vacio")]
        [MaxLength(80)]
        public string Propietario { get; set; }

        public int IdManager { get; set; }
        public List<Manager> Manager { get; set; }
    }
}