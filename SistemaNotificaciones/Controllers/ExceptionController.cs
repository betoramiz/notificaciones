﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SistemaNotificaciones.Controllers
{
    public class ExceptionController : Controller
    {
        // GET: Exception
        public ActionResult NotFound()
        {
            return View();
        }
    }
}