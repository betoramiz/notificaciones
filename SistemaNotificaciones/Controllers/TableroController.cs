﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SistemaNotificaciones.Filters;
using System.Web.Mvc;
using SistemaNotificaciones.Models;
using SistemaNotificaciones.Bussiness;
using SistemaNotificaciones.ViewModel;

namespace SistemaNotificaciones.Controllers
{
    //[Authorize(Roles = "Admin, Normal")]
    [Authorize(Roles = "Admin, Normal")]
    public class TableroController : Controller
    {
        private Contexto contexto = null;
        private ContratoBusiness contratoBl = null;
        private PropiedadBusiness propiedadBl = null;
        private ServicioBussiness servicioBl = null;
        private CobranzaBusiness cobranzaBl = null;

        public TableroController()
        {
            contexto = new Contexto();
            contratoBl = new ContratoBusiness(contexto);
            propiedadBl = new PropiedadBusiness(contexto);
            servicioBl = new ServicioBussiness(contexto);
            cobranzaBl = new CobranzaBusiness(contexto);
        }
        public ActionResult Index()
        {
            List<Contrato> contratos = contratoBl.ReadAll();
            if (contratos == null)
                return View(new TableroVM());
            List<Contrato> contratosVm = (from c in contratos
                                          where c.Activo == true //&& c.NotificacionEnviada == false
                                          select new Contrato()
                                          {
                                              Id  = c.Id,
                                              Activo = c.Activo,
                                              FechaIncio = c.FechaIncio,
                                              FechaFin = c.FechaFin,
                                              Numero = c.Numero,
                                              ServicioId = c.ServicioId,
                                              CobranzaId = c.CobranzaId,
                                              PropiedadId = c.PropiedadId,
                                              Observaciones = c.Observaciones,
                                              RutaArchivos = c.RutaArchivos,
                                              Propiedad = propiedadBl.Read(c.PropiedadId),
                                              Servicio =  servicioBl.Read(c.ServicioId),
                                              Cobranza = cobranzaBl.Read(c.CobranzaId),
                                              Token = c.Token,
                                              NotificacionEnviada = c.NotificacionEnviada
                                          }).ToList();


            var tablero = new TableroVM() {
                Contratos = contratosVm,
                Activos = contratosVm.Where(x => x.Activo == true && (x.FechaFin > DateTime.Today) && (x.FechaFin.Subtract(DateTime.Today).TotalDays > 5)).Count(),
                Vencidos = contratosVm.Where(x => x.FechaFin < DateTime.Today).Count(),
                PorVencer = contratosVm.Where(x => x.FechaFin.Subtract(DateTime.Today).TotalDays <= 5 && x.FechaFin.Subtract(DateTime.Today).TotalDays > 0).Count()
            };
            //var vencer = contratosVm.Where(x => x.FechaFin.Subtract(DateTime.Today).TotalDays <= 5 && x.FechaFin.Subtract(DateTime.Today).TotalDays > 0).ToList();

            return View(tablero);
        }
    }
}