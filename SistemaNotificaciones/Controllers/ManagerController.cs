﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SistemaNotificaciones.Repository;
using SistemaNotificaciones.Models;
using SistemaNotificaciones.Bussiness;
using SistemaNotificaciones.Filters;

namespace SistemaNotificaciones.Controllers
{
    [Authorize(Roles = "Admin")]
    public class ManagerController : Controller
    {
        private ManagerBusiness managerBl = null;
        private Contexto contexto = null;

        public ManagerController()
        {
            contexto = new Contexto();
            managerBl = new ManagerBusiness(contexto);
        }

        public ActionResult Index()
        {
            var managers = managerBl.ReadAll();
            return View(managers);
        }

        public ActionResult Nuevo()
        {
            return View();
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Nuevo(Manager manager)
        {
            var resultado = 0;
            try
            {
                if (!ModelState.IsValid)
                    return View(manager);
                resultado = managerBl.Create(manager);
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("", ex);
            }

            if (resultado == 1)
                return RedirectToAction("Index");
            else
                return View(manager);
        }

        public ActionResult Editar(int id)
        {
            try
            {
                var manager = managerBl.Read(id);
                if (manager != null)
                    return PartialView("EditarPartialView", manager);
                else
                    return RedirectToAction("NotFound", "Exception");
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        [HttpPost]
        public JsonResult Editar(Manager manager)
        {
            var resultado = 0;
            try
            {
                if (!ModelState.IsValid)
                    resultado = 0;

                resultado = managerBl.Update(manager);
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("", ex);
            }
            return Json(resultado);
        }

        public ActionResult Eliminar(int id)
        {
            try
            {
                var manager = managerBl.Read(id);
                if (manager != null)
                    return PartialView("EliminarPartialView", manager);
                else
                    return RedirectToAction("NotFound", "Exception");
            }
            catch (Exception ex)
            {
                return RedirectToAction("Index");
            }
        }

        [HttpPost]
        [ActionName("Eliminar")]
        public JsonResult ConfirmarEliminar(int id)
        {
            try
            {
                var entity = managerBl.Read(id);
                var resultado = managerBl.Delete(entity.Id);
                return Json(resultado, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(ex.Message, JsonRequestBehavior.AllowGet);
            }
        }
    }
}