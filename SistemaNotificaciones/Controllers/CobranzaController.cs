﻿using SistemaNotificaciones.Bussiness;
using SistemaNotificaciones.Filters;
using SistemaNotificaciones.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SistemaNotificaciones.Controllers
{
    [Authorize(Roles = "Admin")]
    public class CobranzaController : Controller
    {
        CobranzaBusiness cobranzaBl = null;
        Contexto contexto = null;

        public CobranzaController()
        {
            contexto = new Contexto();
            cobranzaBl = new CobranzaBusiness(contexto);
        }

        public ActionResult Index()
        {
            var managers = cobranzaBl.ReadAll();
            return View(managers);
        }

        public ActionResult Nuevo()
        {
            return View();
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Nuevo(Cobranza cobranza)
        {
            var resultado = 0;
            try
            {
                if (!ModelState.IsValid)
                    return View(cobranza);
                resultado = cobranzaBl.Create(cobranza);
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("", ex);
            }

            if (resultado == 1)
                return RedirectToAction("Index");
            else
                return View(cobranza);
        }

        public ActionResult Editar(int id)
        {
            try
            {
                var manager = cobranzaBl.Read(id);
                if (manager != null)
                    return PartialView("EditarPartialView", manager);
                else
                    return RedirectToAction("NotFound", "Exception");
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        [HttpPost]
        public JsonResult Editar(Cobranza cobranza)
        {
            var resultado = 0;
            try
            {
                if (!ModelState.IsValid)
                    resultado = 0;

                resultado = cobranzaBl.Update(cobranza);
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("", ex);
            }

            return Json(resultado);
        }

        public ActionResult Eliminar(int id)
        {
            try
            {
                var manager = cobranzaBl.Read(id);
                if (manager != null)
                    return PartialView("EliminarPartialView", manager);
                else
                    return RedirectToAction("NotFound", "Exception");
            }
            catch (Exception ex)
            {
                return RedirectToAction("Index");
            }
        }

        [HttpPost]
        [ActionName("Eliminar")]
        public JsonResult ConfirmarEliminar(int id)
        {
            try
            {
                var entity = cobranzaBl.Read(id);
                var resultado = cobranzaBl.Delete(entity.Id);
                return Json(resultado, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(ex.Message, JsonRequestBehavior.AllowGet);
            }
        }
    }
}