﻿using System;
using System.Web.Mvc;
using SistemaNotificaciones.Repository;
using SistemaNotificaciones.Models;
using SistemaNotificaciones.Bussiness;
using SistemaNotificaciones.ViewModel;
using System.Linq;
using SistemaNotificaciones.Filters;

namespace SistemaNotificaciones.Controllers
{
    [Authorize(Roles = "Admin")]
    public class PropiedadController : Controller
    {
        private Contexto contexto = null;
        private PropiedadBusiness propiedadBl = null;
        private ManagerBusiness managerBl = null;

        public PropiedadController()
        {
            contexto = new Contexto();
            propiedadBl = new PropiedadBusiness(contexto);
            managerBl = new ManagerBusiness(contexto);
        }

        public ActionResult Index()
        {
            var propiedades = propiedadBl.ReadAll();
            var managers = managerBl.ReadAll();
            var result = (from p in propiedades
                          join m in  managers on p.IdManager equals m.Id
                          select new PropiedadVM {
                             Numero = p.Numero,
                             Propietario = p.Propietario,
                             Manager = m.Nombre + " " + m.Apellidos,
                             IdManager = p.IdManager,
                             Id = p.Id
                         }).ToList();

            return View(result);
        }

        public ActionResult Nuevo()
        {
            var propiedad = new Propiedad();
            propiedad.Manager = managerBl.ReadAll();
            return View(propiedad);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Nuevo(Propiedad propiedad)
        {
            var resultado = 0;
            try
            {
                if (!ModelState.IsValid)
                    return View(propiedad);

                resultado = propiedadBl.Create(propiedad);
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("", ex);
            }

            if (resultado == 1)
                return RedirectToAction("Index");
            else
                return View(propiedad);
        }

        public ActionResult Editar(int id)
        {
            try
            {
                var propiedad = propiedadBl.Read(id);
                propiedad.Manager = managerBl.ReadAll();
                if (propiedad != null)
                    return PartialView("EditarPartialView", propiedad);
                else
                    return RedirectToAction("NotFound", "Exception");
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        [HttpPost]
        public JsonResult Editar(Propiedad propiedad)
        {
            var resultado = 0;
            try
            {
                if (!ModelState.IsValid)
                    resultado = 0;

                resultado = propiedadBl.Update(propiedad);
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("", ex);
            }
            return Json(resultado);
        }

        public ActionResult Eliminar(int id)
        {
            try
            {
                var propiedad = propiedadBl.Read(id);
                if (propiedad != null)
                    return PartialView("EliminarPartialView", propiedad);
                else
                    return RedirectToAction("NotFound", "Exception");
            }
            catch (Exception ex)
            {
                return RedirectToAction("Index");
            }
        }

        [HttpPost]
        [ActionName("Eliminar")]
        public JsonResult ConfirmarEliminar(int id)
        {
            try
            {
                var propiedad = propiedadBl.Read(id);
                var resultado = propiedadBl.Delete(propiedad.Id);
                return Json(resultado, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(ex.Message, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult ObtenerPripietario(string numero)
        {
            var propiedad = propiedadBl.FindBy(x => x.Numero == numero).FirstOrDefault();
            if(propiedad != null)
            {
                var managers = managerBl.FindBy(x => x.Id == propiedad.IdManager);
                propiedad.Manager = managers;
            }
            return Json(propiedad, JsonRequestBehavior.AllowGet);
        }
    }
}