﻿using System.Web.Mvc;
using SistemaNotificaciones.ViewModel;
using Microsoft.AspNet.Identity;
using System.Net;
using Microsoft.Owin;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.AspNet.Identity.EntityFramework;
using SistemaNotificaciones.Models;
using System.Web;
using System.Linq;
using System;
using SistemaNotificaciones.Filters;

namespace SistemaNotificaciones.Controllers
{
    [Authorize(Roles = "Admin")]
    public class SecurityController : Controller
    {
        private Contexto contexto = null;
        private UserStore<UsuarioSistema> _userStore = null;
        private UserStore<UsuarioSistema> userStore
        {
            get
            {
                if (contexto == null)
                    contexto = new Contexto();
                if (_userStore == null)
                    _userStore = new UserStore<UsuarioSistema>(contexto);

                return _userStore;
            }
        }

        private UserManager<UsuarioSistema> _userManager = null;
        public UserManager<UsuarioSistema> userManager
        {
            get
            {
                if (_userManager == null)
                    _userManager = new UserManager<UsuarioSistema>(userStore);

                return _userManager;
            }
        }

        private SignInManager<UsuarioSistema, string> _signInManager = null;
        private SignInManager<UsuarioSistema, string> signInManager
        {
            get
            {
                if (_signInManager == null)
                    _signInManager = new SignInManager<UsuarioSistema, string>(userManager, HttpContext.GetOwinContext().Authentication);

                return _signInManager;
            }
        }

        public SecurityController()
        {
            contexto = new Contexto();
        }


        public ActionResult Index()
        {
            var users = userManager.Users.ToList();
            return View(users);
        }

        [AllowAnonymous]
        public ActionResult LogIn()
        {
            var a = HttpContext.User.Identity.IsAuthenticated;
            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        public ActionResult LogIn(LoginVM login)
        {
            if (!ModelState.IsValid)
                return View(login);

            var user = userManager.FindByEmail(login.Email);
            if (user == null)
            {
                ModelState.AddModelError("", "El usuario no existe");
                return View(login);
            }
                

            var resultSignIn = signInManager.PasswordSignIn(login.Email, login.Passowrd, false, false);
            if (resultSignIn == SignInStatus.Success)
                return RedirectToAction("Index", "Tablero");
            else
            {
                ModelState.AddModelError("", "Intento de entrada incorrecto, favor de revisar su usaurio y contrasena");
                return View(login);
            }
                

        }

        [AllowAnonymous]
        public ActionResult SignOut()
        {
            if(User.Identity.IsAuthenticated)
                signInManager.AuthenticationManager.SignOut();
            return RedirectToAction("Index", "Tablero");
        }

        [Authorize(Roles = "Admin")]
        public ActionResult Nuevo()
        {
            var roles = contexto.Roles.ToList() ;

            NuevoUsuario nuevo = new NuevoUsuario()
            {
                Roles = roles
            };
            return View(nuevo);
        }

        [HttpPost]
        [Authorize(Roles = "Admin")]
        public ActionResult Nuevo(SistemaNotificaciones.ViewModel.NuevoUsuario usuario)
        {
            if (!ModelState.IsValid)
                return View(usuario);

            var existUser = userManager.FindByEmail(usuario.Email) == null ? false : true;

            if (existUser)
            {
                ModelState.AddModelError("", "El usuario ya esta registrado");
                return View(usuario);
            }

            var roleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(contexto));
            var role = roleManager.FindById(usuario.RoleId.ToString());


            var usuarioSistema = new UsuarioSistema() { UserName = usuario.Email, Email = usuario.Email };
            var createdUser = userManager.Create(usuarioSistema, usuario.Password);
            if (createdUser.Succeeded)
            {
                userManager.AddToRole(usuarioSistema.Id, role.Name);
                signInManager.SignIn(usuarioSistema, false, false);
                return RedirectToAction("Index", "Tablero");
            }
            else
                return View(usuario);
        }

        public ActionResult Eliminar(string id)
        {
            var usuario = userManager.FindById(id);
            if(usuario == null)
            {
                ModelState.AddModelError("", "El usuario a eliminar no existe");
                return RedirectToAction("Index");
            }


            return View(usuario);
        }

        [HttpPost]
        [ActionName("Eliminar")]
        public ActionResult ConfirmarEliminar(string id)
        {
            var usuario = userManager.FindById(id);
            try
            {
                userManager.Delete(usuario);
            }
            catch(Exception ex)
            {
                ModelState.AddModelError("", ex.Message);
            }
            
            return RedirectToAction("Index");
        }

        [AllowAnonymous]
        public ActionResult NoAutorizado(string returnUrl)
        {
            Response.StatusCode = 403;
            //ViewBag.returnUrl = returnUrl;
            return View();
        }
    }
}