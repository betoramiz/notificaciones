﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SistemaNotificaciones.Repository;
using SistemaNotificaciones.Models;
using SistemaNotificaciones.Bussiness;
using SistemaNotificaciones.ViewModel;
using SistemaNotificaciones.Filters;

namespace SistemaNotificaciones.Controllers
{
    [Authorize(Roles = "Admin")]
    public class ServicioController : Controller
    {
        private ServicioBussiness servicioBl = null;
        private Contexto contexto = null;
        
        public ServicioController()
        {
            contexto = new Contexto();
            servicioBl = new ServicioBussiness(contexto);
        }

        public ActionResult Index()
        {
            var servicios = servicioBl.ReadAll();
            return View(servicios);
        }

        public ActionResult Nuevo()
        {
            var mediciones = new List<string>();
            mediciones.Add("Dias");
            mediciones.Add("Semanas");
            mediciones.Add("Meses");
            var servicio = new ServicioVM();
            servicio.Medicion = mediciones;
            return View(servicio);
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Nuevo(Servicio servicio)
        {
            var resultado = 0;
            try
            {
                if(!ModelState.IsValid)
                    return View(servicio);
                resultado = servicioBl.Create(servicio);
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("", ex);
            }

            if (resultado == 1)
                return RedirectToAction("Index");
            else
                return View(servicio);
        }

        public ActionResult Editar(int id)
        {
            try
            {
                var servicio = servicioBl.Read(id);
                var mediciones = new List<string>();
                mediciones.Add("Dia");
                mediciones.Add("Semana");
                mediciones.Add("Mes");
                var servicioVm = new ServicioVM() {
                    Id = servicio.Id,
                    Nombre = servicio.Nombre,
                    Duracion = servicio.Duracion,
                    Medicion = mediciones
                };
                if (servicio != null)
                    return PartialView("EditarPartialView", servicioVm);
                else
                    return RedirectToAction("NotFound", "Exception");
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        [HttpPost]
        public JsonResult Editar(ServicioVM servicioVm)
        {
            var resultado = 0;
            try
            {
                var servicio = new Servicio() {
                    Id = servicioVm.Id,
                    Nombre = servicioVm.Nombre,
                    Duracion = servicioVm.Duracion,
                    Medicion = servicioVm.MedicioId.ToString()
                };

                if (!ModelState.IsValid)
                    return Json(0, JsonRequestBehavior.AllowGet);

                resultado = servicioBl.Update(servicio);
                
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("", ex);
            }

            if (resultado == 1)
                return Json(resultado, JsonRequestBehavior.AllowGet);
            else
                return Json(resultado, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Eliminar(int id)
        {
            try
            {
                var servicio = servicioBl.Read(id);
                if (servicio != null)
                    return PartialView("EliminarPartialView", servicio);
                else
                    return RedirectToAction("NotFound", "Exception");
            }
            catch (Exception ex)
            {
                return RedirectToAction("Index");
            }
        }

        [HttpPost]
        [ActionName("Eliminar")]
        public JsonResult ConfirmarEliminar(int id)
        {
            try
            {
                var entity = servicioBl.Read(id);
                var resultado = servicioBl.Delete(entity.Id);
                return Json(resultado, JsonRequestBehavior.AllowGet);
            }
            catch(Exception ex)
            {
                return Json(ex.Message, JsonRequestBehavior.AllowGet);
            }
        }
    }
}