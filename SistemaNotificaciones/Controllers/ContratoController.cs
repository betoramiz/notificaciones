﻿using SistemaNotificaciones.Bussiness;
using SistemaNotificaciones.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SistemaNotificaciones.ViewModel;
using AutoMapper;
using SistemaNotificaciones.Filters;
using System.Text;
using System.Net.Mail;
using System.Data.SqlClient;
using System.Configuration;
using System.Data;
using System.Net.Mime;
using System.IO;

namespace SistemaNotificaciones.Controllers
{
    [Authorize(Roles = "Admin")]
    public class ContratoController : Controller
    {

        private ContratoBusiness contratoBl = null;
        private Contexto contexto = null;
        private ServicioBussiness servicioBl = null;
        private CobranzaBusiness cobranzaBl = null;
        private PropiedadBusiness propiedadBl = null;
        private ManagerBusiness managerBl = null;
        private RenovacionBusiness renovacionBl = null;

        public ContratoController()
        {
            contexto = new Contexto();
            contratoBl = new ContratoBusiness(contexto);
            servicioBl = new ServicioBussiness(contexto);
            cobranzaBl = new CobranzaBusiness(contexto);
            propiedadBl = new PropiedadBusiness(contexto);
            managerBl = new ManagerBusiness(contexto);
            renovacionBl = new RenovacionBusiness(contexto);
        }

        public ActionResult Index()
        {
            var contratos = contratoBl.ReadAll();
            var contratoIndex = (from c in contratos
                                 select new Contrato()
                                 {
                                     Servicio = servicioBl.FindBy(x => x.Id == c.ServicioId).FirstOrDefault(),
                                     Cobranza = cobranzaBl.FindBy(x => x.Id == c.CobranzaId).FirstOrDefault(),
                                     Propiedad = propiedadBl.FindBy(x => x.Id == c.PropiedadId).FirstOrDefault()
                                 }).ToList();

            return View(contratos);
        }

        public ActionResult Nuevo()
        {
            var contrato = new ContratoVM();
            contrato.Activo = true;
            contrato.FechaIncio = DateTime.Today;
            contrato.Servicios = servicioBl.ReadAll();
            contrato.Cobranzas = cobranzaBl.ReadAll();
            contrato.Propiedades = propiedadBl.ReadAll();

            return View(contrato);
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Nuevo(ContratoVM contratoVm)
        {
            var resultado = 0;
            try
            {
                if (!ModelState.IsValid)
                {
                    contratoVm.Propiedades = propiedadBl.ReadAll();
                    contratoVm.Servicios = servicioBl.ReadAll();
                    contratoVm.Cobranzas = cobranzaBl.ReadAll();
                    return View(contratoVm);
                }
                    


                var fechainicio = contratoVm.FechaIncio.ToShortDateString();
                var random = new Random().Next(1, 100);
                var contrato = new Contrato()
                {
                    Numero = contratoVm.Numero,
                    FechaIncio = contratoVm.FechaIncio,
                    Servicio = servicioBl.FindBy(x => x.Id == contratoVm.servicioId).FirstOrDefault(),
                    FechaFin = contratoVm.FechaFin,
                    Observaciones = contratoVm.Observaciones ?? string.Empty,
                    Activo = contratoVm.Activo,
                    Propiedad = propiedadBl.FindBy(x => x.Numero == contratoVm.NumeroPropiedad).FirstOrDefault(),
                    Cobranza = cobranzaBl.FindBy(x => x.Id == contratoVm.cobranzaId).FirstOrDefault(),
                    RutaArchivos = contratoVm.RutaArchivos ?? string.Empty,
                    Id = 0,
                    Token = contratoVm.FechaIncio.ToString("yyyyMMdd") + contratoVm.FechaFin.ToString("yyyyMMdd") + contratoVm.Numero + random.ToString()
                };

                resultado = contratoBl.Create(contrato);
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("", ex);
            }

            if (resultado == 1)
                return RedirectToAction("Index");
            else
                return View(contratoVm);
        }

        public ActionResult Editar(int id)
        {
            try
            {
                var contrato = contratoBl.Read(id);
                var propiedad = propiedadBl.FindBy(x => x.Id == contrato.PropiedadId).FirstOrDefault();
                ContratoVM contratoVm = new ContratoVM()
                {
                    Id = contrato.Id,
                    Activo = contrato.Activo,
                    cobranzaId = contrato.CobranzaId,
                    FechaFin = contrato.FechaFin,
                    Numero = contrato.Numero,
                    RutaArchivos = contrato.RutaArchivos,
                    servicioId = contrato.ServicioId,
                    Observaciones = contrato.Observaciones,
                    NumeroPropiedad = propiedad.Numero,
                    FechaIncio = contrato.FechaIncio,
                    Servicios = servicioBl.ReadAll(),
                    Cobranzas = cobranzaBl.ReadAll(),
                    Propiedades = propiedadBl.ReadAll(),
                };

                //var propiedad = propiedadBl.Read(int.Parse(contratoVm.NumeroPropiedad));
                ViewBag.Propietario = propiedad.Propietario;
                var manager = managerBl.Read(propiedad.IdManager);
                if (manager != null)
                    ViewBag.Manager = manager.Nombre + " " + manager.Apellidos;
                else
                    ViewBag.Manager = null;
                ViewBag.FechaInicio = String.Format("{0: yyyy-MM-dd}",contratoVm.FechaIncio);
                ViewBag.FechaFin = String.Format("{0: yyyy-MM-dd}", contratoVm.FechaFin);

                if (contratoVm != null)
                    return PartialView("EditarPartialView", contratoVm);
                else
                    return RedirectToAction("NotFound", "Exception");
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        [HttpPost]
        public JsonResult Editar(ContratoVM contratoVm)
        {
            var resultado = 0;
            try
            {
                if (!ModelState.IsValid)
                    resultado = 0;

                var propiedad = propiedadBl.FindBy(x => x.Numero == contratoVm.NumeroPropiedad).FirstOrDefault();
                var contrato = new Contrato()
                {
                    Id = contratoVm.Id,
                    FechaIncio = contratoVm.FechaIncio,
                    FechaFin = contratoVm.FechaFin,
                    ServicioId = contratoVm.servicioId,
                    Activo = contratoVm.Activo,
                    Numero = contratoVm.Numero,
                    Observaciones = contratoVm.Observaciones,
                    CobranzaId = contratoVm.cobranzaId,
                    PropiedadId = propiedad.Id
                };

                if (!ModelState.IsValid)
                    resultado = 0;

                resultado = contratoBl.Update(contrato);
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("", ex);
            }

            return Json(resultado);
        }

        public ActionResult Eliminar(int id)
        {
            try
            {
                var servicio = contratoBl.Read(id);
                if (servicio != null)
                    return PartialView("EliminarPartialView", servicio);
                else
                    return RedirectToAction("NotFound", "Exception");
            }
            catch (Exception ex)
            {
                return RedirectToAction("Index");
            }
        }

        [HttpPost]
        [ActionName("Eliminar")]
        public JsonResult ConfirmarEliminar(int id)
        {
            try
            {
                var entity = contratoBl.Read(id);
                var resultado = contratoBl.Delete(entity.Id);
                return Json(resultado, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(ex.Message, JsonRequestBehavior.AllowGet);
            }
        }


        /// <summary>
        /// Metodo que recibe la respuesta por parte del cliente si va a Renovar contrato o no
        /// </summary>
        /// <param name="token"></param>
        /// <param name="contrato"></param>
        /// <param name="respuesta"></param>
        /// <returns></returns>
        [HttpGet]
        [AllowAnonymous]
        public ActionResult Renovar(string token, string contrato, int respuesta)
        {
            if(respuesta == 1)
            {
                var servicios = servicioBl.ReadAll();
                RenovarVM renovar = new RenovarVM();
                renovar.Token = token;
                renovar.Contrato = contrato;
                renovar.Servicios = servicios;
                renovar.ServicioId = 0;

                ViewBag.respuesta = 1;
                return View(renovar);
            }
            else
            {
                Renovacion renovacion = new Renovacion()
                {
                    ContratoId = int.Parse(contrato),
                    FechaRenovacion = DateTime.Today,
                    Renovo = false

                };
                var resultadoRenovacion = renovacionBl.Create(renovacion);
                ViewBag.respuesta = 2;
            }

            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        public JsonResult Renovar(string contrato)
        {
            //enviar correo de renovacino al admin del sistema
            var contratoRenovacion = contratoBl.Read(int.Parse(contrato));
            var propiedad = propiedadBl.FindBy(x => x.Id == contratoRenovacion.PropiedadId).FirstOrDefault();
            var manager = managerBl.Read(propiedad.IdManager);

            EnviarNotificacion notificacion = new ViewModel.EnviarNotificacion()
            {
                IdContrato = contratoRenovacion.Id,
                NombrePropietario = propiedad.Propietario,
                NumeroPropiedad = propiedad.Numero,
                Email = manager.Email,
                Mensaje = string.Empty
            };
            Renovacion renovacion = new Renovacion()
            {
                ContratoId = int.Parse(contrato),
                FechaRenovacion = DateTime.Today,
                Renovo = true
            };
            renovacionBl.Create(renovacion);

            var emailFrom = ConfigurationManager.AppSettings["Email"].ToString();
            var enviado = EnviarCorreoAceptacion(emailFrom, notificacion);
            if (enviado)
                return Json(notificacion);
            else
                return Json(null);
            //return Json(notificacion);
        }

        /// <summary>
        /// Metodo para Mostrar el formulario de envio de notificacion al Administrador de la pripiedad
        /// </summary>
        /// <param name="token"></param>
        /// <param name="idContrato"></param>
        /// <returns></returns>
        [HttpGet]
        public ActionResult EnviarNotificacion(string token, int idContrato)
        {
            var contrato = contratoBl.Read(idContrato);
            var propiedad = propiedadBl.FindBy(x => x.Id == contrato.PropiedadId).FirstOrDefault();
            var manager = managerBl.Read(propiedad.IdManager);
            EnviarNotificacion notificacion = new ViewModel.EnviarNotificacion() {
                IdContrato = contrato.Id,
                NombrePropietario = propiedad.Propietario,
                NumeroPropiedad = propiedad.Numero,
                Email = manager.Email,
                Mensaje = string.Empty
            };

            return View(notificacion);
        }

        /// <summary>
        /// Metodo que envia el correo de notificacion al administrador de la propiedad
        /// </summary>
        /// <param name="notificacion"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult EnviarNotificacion(EnviarNotificacion notificacion)
        {
            var contrato = contratoBl.Read(notificacion.IdContrato);
            var propiedad = propiedadBl.FindBy(x => x.Id == contrato.PropiedadId).FirstOrDefault();
            var manager = managerBl.Read(propiedad.IdManager);
            contrato.NotificacionEnviada = true;

            var seEnvio = EnviarCorreo(manager.Email, propiedad.Propietario, notificacion.FileAttached, notificacion.Token, notificacion.IdContrato.ToString(), notificacion.Mensaje);
            if(seEnvio)
            {
                contrato.NotificacionEnviada = true;
                var result = contratoBl.Update(contrato);
            }
                
            return RedirectToAction("Index", "Tablero");
        }



        public static bool EnviarCorreo(string emailTo, string nombreCliente, HttpPostedFileBase archivo, string token, string contrato, string mensaje)
        {
            bool resultado = false;
            try
            {
                var emailFrom = ConfigurationManager.AppSettings["Email"].ToString();
                var password = ConfigurationManager.AppSettings["EmailPassword"].ToString();

                var emailTemplateUrl = @"C:\Users\Beto\documents\visual studio 2015\Projects\ControlDePagos\SistemaNotificaciones\Template\Email.html";
                var urlRenovar = "http://localhost:62371/Contrato/Renovar?token=" + token + "&contrato=" + contrato + "&respuesta=1";
                var urlNoRenovar = "http://localhost:62371/Contrato/Renovar?token=" + token + "&contrato=" + contrato + "&respuesta=2";
                StreamReader reader = new StreamReader(emailTemplateUrl);
                var email = reader.ReadToEnd();

                email = email.Replace("{_cliente}", nombreCliente);
                email = email.Replace("{_contrato}", contrato);
                email = email.Replace("{_urlRenovacion}", urlRenovar);
                email = email.Replace("{_urlNoRenovacion}", urlNoRenovar);
                email = email.Replace("{_mensaje}", mensaje);

                Attachment data = null;
                if (archivo != null && archivo.ContentLength > 0)
                {
                    data = new Attachment(archivo.InputStream, Path.GetFileName(archivo.FileName), MediaTypeNames.Application.Octet);
                }
                    

                MailMessage mailMessage = new MailMessage();
                MailAddress fromAddress = new MailAddress(emailFrom);
                mailMessage.From = fromAddress;
                mailMessage.To.Add(emailTo);
                mailMessage.Body = email;
                mailMessage.BodyEncoding = System.Text.Encoding.UTF8;
                mailMessage.IsBodyHtml = true;
                mailMessage.Subject = "Notificacion de Renovacion";
                if(data != null)
                    mailMessage.Attachments.Add(data);

                SmtpClient smtpClient = new SmtpClient("smtp.gmail.com");
                smtpClient.Credentials = new System.Net.NetworkCredential(emailFrom, password);
#if DEBUG
                smtpClient.Host = "smtp.gmail.com";
                smtpClient.Port = 587;
                smtpClient.EnableSsl = true;
#else
                smtpClient.Host = "mail.serverdominio.com";
                smtpClient.EnableSsl = true;                
#endif
                smtpClient.Send(mailMessage);

                resultado = true;

            }
            catch (Exception ex)
            {
                resultado = false;
            }
            return resultado;
        }

        public static bool EnviarCorreoAceptacion(string emailTo, EnviarNotificacion notificacion)
        {
            bool resultado = false;
            try
            {
                var emailFrom = ConfigurationManager.AppSettings["Email"].ToString();
                var password = ConfigurationManager.AppSettings["EmailPassword"].ToString();

                StringBuilder cuerpoEmail = new StringBuilder();
                cuerpoEmail.Append("<p>El cliente " + notificacion.NombrePropietario + " ha decidio renovar el contrato " + notificacion.NumeroPropiedad + "</p>");

                MailMessage mailMessage = new MailMessage();
                MailAddress fromAddress = new MailAddress(emailFrom);
                mailMessage.From = fromAddress;
                mailMessage.To.Add(emailTo);
                mailMessage.Body = cuerpoEmail.ToString();
                mailMessage.BodyEncoding = System.Text.Encoding.UTF8;
                mailMessage.IsBodyHtml = true;
                mailMessage.Subject = " Notificacion de Renovacion";
                SmtpClient smtpClient = new SmtpClient("smtp.gmail.com");
                smtpClient.Credentials = new System.Net.NetworkCredential(emailFrom, password);
#if DEBUG
                smtpClient.Host = "smtp.gmail.com";
                smtpClient.Port = 587;
                smtpClient.EnableSsl = true;
#else
                smtpClient.Host = "mail.serverdominio.com";
                smtpClient.EnableSsl = true;                
#endif
                smtpClient.Send(mailMessage);

                resultado = true;

            }
            catch (Exception ex)
            {
                resultado = false;
            }
            return resultado;
        }
    }
}