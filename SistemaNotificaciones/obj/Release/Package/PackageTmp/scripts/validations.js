﻿var valdiateService = (function () {
    validate = function (tagContainer, rules, messages) {
       return $(tagContainer).validate({
            rules: rules,
            messages : messages
        });
    }

    return {
        validateForm: validate
    }

})();