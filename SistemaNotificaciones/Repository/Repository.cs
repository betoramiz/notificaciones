﻿using SistemaNotificaciones.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;

namespace SistemaNotificaciones.Repository
{
    public class Repository<T> : IRepository<T> where T : class
    {
        protected Contexto context;
        protected DbSet<T> db;
        public Repository(Contexto context)
        {
            this.context = context;
            db = context.Set<T>();
        }

        public virtual int Create(T Entity)
        {
            try
            {
                db.Add(Entity);
                return Save();
            }
            catch (Exception ex)
            {
                return 0;
            }
        }

        public virtual int Delete(int Id)
        {
            try
            {
                var entity = db.Find(Id);
                db.Remove(entity);
                return Save();
            }
            catch (Exception)
            {
                return 0;
            }
            
        }

        public virtual T Read(int Id)
        {
            try
            {
                var entity = db.Find(Id);
                return entity;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public virtual List<T> ReadAll()
        {
            try
            {
                return db.ToList();
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public virtual List<Result> GetAll<Result>(Expression<Func<T, Result>> selector) where Result : class
        {
            try
            {
                return db.Select(selector).ToList();
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public virtual List<T> FindBy(Expression<Func<T, bool>> predicate)
        {
            try
            {
                return db.Where(predicate).ToList();
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public virtual int Update(T Entity)
        {
            try
            {
                context.Entry(Entity).State = EntityState.Modified;
                return Save();
            }
            catch (Exception ex)
            {
                return 0;
            }
        }

        public int Save()
        {
            return context.SaveChanges();
        }
    }
}