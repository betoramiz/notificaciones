﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace SistemaNotificaciones.Repository
{
    public interface IRepository<T> where T : class
    {
        int Create(T Entity);
        T Read(int Id);
        List<T> ReadAll();
        List<T> FindBy(Expression<Func<T, bool>> predicate);
        int Update(T Entity);
        int Delete(int Id);
    }
}
