﻿var repository = (function () {

    var asyncCall = function (url, method, data, dataType) {
        return $.ajax({
            url: url,
            method: method,
            data: data,
            dataType: dataType
        });
    }

    return {
        asyncCall: asyncCall
    }
})();